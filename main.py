import rasterio.mask
import rasterio.features
import rasterio.warp
import fiona
import os
from shapely.geometry import LineString
from urllib.request import urlopen
from tkinter import *
from datetime import datetime
from osgeo import gdal, ogr
from WindowManager import WindowManager
# For pyinstaller
from rasterio import _shim, control, sample, vrt
from fiona import _shim, _err, schema
from fiona.ogrext import *
from pyproj import _datadir, datadir
import numpy.random.common
import numpy.random.mtrand
import numpy.random.bounded_integers
import numpy.random.mt19937

# Constants #
ELEVATION_FIELD = "elev"


def getCurrentTime():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d_%H-%M-%S")


# function to generate .prj file information using spatialreference.org
def getWKT_PRJ (epsg_code):
    # access projection information
    with urlopen("http://spatialreference.org/ref/epsg/{0}/prettywkt/".format(epsg_code)) as wkt:
        html_response = wkt.read()
        encoding = wkt.headers.get_content_charset("utf-8")
        decoded_html = html_response.decode(encoding)

        # remove spaces between characters
        remove_spaces = decoded_html.replace(" ", "")
        # place all the text on one line
        output = remove_spaces.replace("\n", "")
        return output


def getContours(dem, field):
    # Read Digital Elevation Model #
    driver_dem = gdal.Open(dem)
    dem = driver_dem.GetRasterBand(1)
    no_data_value = dem.GetNoDataValue()

    # Read field an create new layer #
    ogr_contour = ogr.GetDriverByName("ESRI Shapefile").Open(field, 1)
    contour_filename_current_time = "contour-" + getCurrentTime()
    contour_shp = ogr_contour.CreateLayer(contour_filename_current_time)

    # Create Projection File #
    createProjectionFile(field, contour_filename_current_time)

    # Adding attributes to layer #
    field_def = ogr.FieldDefn("ID", ogr.OFTInteger)
    contour_shp.CreateField(field_def)
    field_def = ogr.FieldDefn(ELEVATION_FIELD, ogr.OFTReal)
    contour_shp.CreateField(field_def)

    gdal.ContourGenerate(dem, 1, 0, [], 1, no_data_value, contour_shp, 0, 1)

    # Cleanup #
    del ogr_contour
    del dem

    return os.path.dirname(field) + "/" + contour_filename_current_time + ".shp"


def createProjectionFile(field, filename):
    projection_path = field.rsplit("/", 1)[0]
    projection_path = projection_path + "/" + filename + ".prj"
    projection = open(projection_path, "w")
    projection.write(getWKT_PRJ("4326"))
    projection.close()


def clipDemField(path_digital_elevation_model, path_field):
    path_digital_elevation_model_clipped = getFilepath(path_digital_elevation_model)

    with fiona.open(path_field, "r") as input_shape:
        features = [feature["geometry"] for feature in input_shape]

    with rasterio.open(path_digital_elevation_model) as src:
        out_image, out_transform = rasterio.mask.mask(src, features, crop=True)
        out_meta = src.meta.copy()

        out_meta.update({"driver": "GTiff",
                         "height": out_image.shape[1],
                         "width": out_image.shape[2],
                         "transform": out_transform})

    with rasterio.open(path_digital_elevation_model_clipped, "w", **out_meta) as dest:
        dest.write(out_image)


def getFilepath(path):
    return str(path.rsplit("/", 1)[0]) + "/dem_clipped.tif"


def calculateMultipleContours(path, dem_offset):
    shp_reduced_contours_path = os.path.splitext(path)[0] + "_" + str(dem_offset) + "m.shp"
    print(dem_offset)
    with fiona.open(path) as input_shape:
        limit = input_shape.get(0)["properties"][ELEVATION_FIELD]
        limit_offset = dem_offset

        # The output has the same schema
        output_schema = input_shape.schema.copy()

        # write a new shapefile
        with fiona.open(shp_reduced_contours_path, 'w', 'ESRI Shapefile', output_schema,
                        crs=input_shape.crs) as output_shape:
            for elem in input_shape:
                if elem["properties"][ELEVATION_FIELD] <= (limit - limit_offset) or \
                        elem["properties"][ELEVATION_FIELD] >= (limit + limit_offset):
                    limit = elem["properties"][ELEVATION_FIELD]

                    ### Simplify contour lines with Ramer–Douglas–Peucker algorithm###
                    line = LineString(elem["geometry"]["coordinates"])
                    lineShaped = line.simplify(0.00001999, preserve_topology=False)
                    elem["geometry"]["coordinates"] = list(lineShaped.coords)

                    ### Write to shapefile ###
                    output_shape.write(elem)
                else:
                    print("Offset is too small!")


if __name__ == "__main__":
    # createMainWindow()
    root_window = Tk()

    ### General TKinter settings ###
    root_window.title("Höhenschichtlinien berechnen")
    root_window.iconbitmap("C:/temp/pycharm/optEro/icons/kartoffeln.ico")

    window = WindowManager(root_window)

    if window.getPathDem() is None and window.getPathField() is None:
        clipDemField(window.getPathDem(), window.getPathField())
        contour_shapefile_path = getContours(getFilepath(window.getPathDem()), window.getPathField())

        # plotShapefile(contour_shapefile_path)
        calculateMultipleContours(contour_shapefile_path, window.getChoosenDemOffset())
