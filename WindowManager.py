import os
import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling
from tkinter.filedialog import askopenfile, askopenfilename
from tkinter import messagebox
from tkinter import *


class WindowManager:
    root_window = None
    path_digital_elevation_model = ""
    path_field = ""
    choosen_dem_offset = None

    def __init__(self, window):
        self.root_window = window
        self.root_window.resizable(False, False)
        self.root_window.protocol("WM_DELETE_WINDOW", self.on_closing)
        frameButtons = Frame(self.root_window)
        frameButtons.pack()

        frameDemOffset = Frame(self.root_window)
        frameDemOffset.pack()

        self.setChoosenOffset(IntVar(self.root_window))
        current_offset = IntVar(self.root_window)

        ### Buttons ###
        btn_dhm = Button(master=frameButtons,
                         text="Auswahl des Höhenmodells",
                         command=lambda: self.selectFile("dem"))
        btn_dhm.pack(side="top",
                     fill="x",
                     padx="1")

        btn_field = Button(master=frameButtons,
                           text="Auswahl der Feldgrenzen",
                           command=lambda: self.selectFile("field"))
        btn_field.pack(side="top",
                       fill="x",
                       pady="5")

        ### Spinbox ###
        spinBox_Offset = Spinbox(master=frameDemOffset,
                                 from_=1,
                                 to=100,
                                 textvariable=current_offset,
                                 state="readonly",
                                 command=lambda: self.setChoosenOffset(current_offset))
        spinBox_Offset.pack(side="right",
                            fill="both")

        ### Textbox ###
        txtBox_OffsetDesc = Text(master=frameDemOffset,
                                 height=1,
                                 width=40)
        txtBox_OffsetDesc.insert(INSERT,
                                 "Abstand der Höhenschichtlinien in Meter:")
        txtBox_OffsetDesc.config(state=DISABLED)
        txtBox_OffsetDesc.pack(side="left",
                               fill="both")

        ### Start calculation and close window Button ###
        btn_exit = Button(master=self.root_window,
                          text="Höhenschichtlinien berechnen",
                          command=self.root_window.destroy)
        btn_exit.pack(side="bottom",
                      fill="both",
                      pady="20")

        self.root_window.mainloop()


    def selectFile(self, param):
        if param is "dem":
            self.path_digital_elevation_model = askopenfilename()
            self.path_digital_elevation_model = self.switchGTiffCrs(self.path_digital_elevation_model)
        elif param is "field":
            self.path_field = askopenfilename()

    def getChoosenDemOffset(self):
        if self.choosen_dem_offset.get() == 0:
            self.choosen_dem_offset.set(1)
            return self.choosen_dem_offset.get()
        else:
            return self.choosen_dem_offset.get()

    def getPathDem(self):
        return self.path_digital_elevation_model

    def getPathField(self):
        return self.path_field

    def setChoosenOffset(self, spinboxValue):
        self.choosen_dem_offset = spinboxValue

    def checkGtiffProjection(self, file):
        with rasterio.open(file) as src:
            os.system('gdalwarp infile.tif outfile.tif -t_srs "+proj=longlat +ellps=WGS84"')

    def switchGTiffCrs(self, inpath):
        dst_crs = "EPSG:4326"
        outpath = inpath.rsplit(".", 1)[0]
        outpath = outpath + "_wgs84.tif"

        if not os.path.isfile(outpath):
            with rasterio.open(inpath) as src:
                if src.crs.data['init'] != "epsg:4326":
                    transform, width, height = calculate_default_transform(
                        src.crs, dst_crs, src.width, src.height, *src.bounds)
                    kwargs = src.meta.copy()
                    kwargs.update({
                        'crs': dst_crs,
                        'transform': transform,
                        'width': width,
                        'height': height
                    })

                    with rasterio.open(outpath, 'w', **kwargs) as dst:
                        for i in range(1, src.count + 1):
                            reproject(
                                source=rasterio.band(src, i),
                                destination=rasterio.band(dst, i),
                                src_transform=src.transform,
                                src_crs=src.crs,
                                dst_transform=transform,
                                dst_crs=dst_crs,
                                resampling=Resampling.nearest)
                    return outpath
        else:
            return outpath

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Programm beenden?"):
            self.root_window.destroy()




